<?php

class M_hobi extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  public function get_all_hobi()
  {
    $query = $this->db->get('ref_hobi');
    return $query->result_array();
  }

  public function tambah_hobi($data)
  {
    return $this->db->insert('mahasiswa_hobi', $data);
  }
}
